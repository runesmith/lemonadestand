//
//  Mixture.swift
//  LemonadeStand
//
//  Created by Thordur Thordarson on 2014-12-27.
//  Copyright (c) 2014 Thordur Thordarson. All rights reserved.
//

import Foundation

public class Mixture {
    private var mixtureItems: [Item]
    private var mixtureAmounts: [Int]
    
    public init(mixtureItems: Item...) {
        self.mixtureItems = mixtureItems
        self.mixtureAmounts = []
        for (var i = 0; i < mixtureItems.count; i++) {
            self.mixtureAmounts.insert(0, atIndex: i)
        }
    }
    
    public func addToMixture(item: Item, amount: Int) -> ValidationResult {
        if (item.count < amount) {
            return ValidationResult(resultType: ValidationResultType.Error, message: "Cannot add \(item.text) to mixture. You don't have enough of it.")
        }
        
        if (!mixtureItems.contains(item)) {
            return ValidationResult(resultType: ValidationResultType.Error, message: "\(item.text) is not defined in the mixture.")
        }
        
        var index = mixtureItems.indexOf(item)
        
        if ((mixtureAmounts[index] + amount) < 0) {
            return ValidationResult(resultType: ValidationResultType.Error, message: "The mixture doesn't have any more of \(item.text) to remove from it.")
        }
        
        item.count -= amount
        mixtureAmounts[index] += amount
        
        return ValidationResult(resultType: ValidationResultType.Success)
    }
    
    public func removeFromMixture(item: Item, amount: Int) -> ValidationResult {
        return addToMixture(item, amount: -amount)
    }
    
    public func getAmount(item: Item) -> Int {
        var index = mixtureItems.indexOf(item)
        
        return mixtureAmounts[index]
    }
    
    public func getRatioOfItem(item: Item) -> Double {
        var itemAmount = getAmount(item)
        var otherAmount = mixtureAmounts.sum() - itemAmount
        
        if (itemAmount == 0 || otherAmount == 0) {
            return 0.0
        }
        
        return Double(itemAmount) / Double(otherAmount)
    }
}