//
//  Customer.swift
//  LemonadeStand
//
//  Created by Thordur Thordarson on 2014-12-28.
//  Copyright (c) 2014 Thordur Thordarson. All rights reserved.
//

import Foundation

public class Customer {
    private var lemonadePreferenceNumber: Int
    public var preference: LemonadeType {
        get {
            if (lemonadePreferenceNumber >= 0 && lemonadePreferenceNumber < 4) {
                return LemonadeType.Acidic
            }
            else if (lemonadePreferenceNumber >= 4 && lemonadePreferenceNumber < 6) {
                return LemonadeType.EqualPortioned
            }
            else {
                return LemonadeType.Diluted
            }
        }
    }
    
    init () {
        self.lemonadePreferenceNumber = Random.getNumber(0, maxNumber: 10)
    }
}