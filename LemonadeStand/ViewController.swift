//
//  ViewController.swift
//  LemonadeStand
//
//  Created by Thordur Thordarson on 2014-12-27.
//  Copyright (c) 2014 Thordur Thordarson. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    private var game: LemonadeStandGame = LemonadeStandGame()
    private var toast: Toast<ViewController>? = nil
    
    @IBOutlet weak var dayLbl: UILabel!
    @IBOutlet weak var weatherImg: UIImageView!
    @IBOutlet weak var inventoryMoneyLbl: UILabel!
    @IBOutlet weak var inventoryLemonsLbl: UILabel!
    @IBOutlet weak var inventoryIceCubesLbl: UILabel!
    @IBOutlet weak var purchasedLemonsLbl: UILabel!
    @IBOutlet weak var purchasedIceCubesLbl: UILabel!
    @IBOutlet weak var mixedLemonsLbl: UILabel!
    @IBOutlet weak var mixedIceCubesLbl: UILabel!
    
    @IBAction func buyLemonAction(sender: AnyObject) {
        callFunctionWithValidationResult(game.buyLemon)
    }
    @IBAction func buyIceCubesAction(sender: AnyObject) {
        callFunctionWithValidationResult(game.buyIceCubes)
    }
    
    @IBAction func sellLemonAction(sender: AnyObject) {
        callFunctionWithValidationResult(game.sellLemon)
    }
    
    @IBAction func sellIceCubesAction(sender: AnyObject) {
        callFunctionWithValidationResult(game.sellIceCubes)
    }
    
    @IBAction func addLemonMixAction(sender: AnyObject) {
        callFunctionWithValidationResult(game.addLemonToMix)
    }
    
    @IBAction func addIceCubesMixAction(sender: AnyObject) {
        callFunctionWithValidationResult(game.addIceCubesToMix)
    }
    
    @IBAction func removeLemonMixAction(sender: AnyObject) {
        callFunctionWithValidationResult(game.removeLemonFromMix)
    }
    
    @IBAction func removeIceCubesMixAction(sender: AnyObject) {
        callFunctionWithValidationResult(game.removeIceCubesFromMix)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        toast = Toast(viewController: self)
        
        updateUI()
    }
    
    @IBAction func startDayAction(sender: AnyObject) {
        var result = game.simulateDay()
        
        var header = result.resultType == GameResultType.Proceed
            ? "You finished todays day"
            : "Game Over"
        
        toast?.info(header: header, message: result.message)
        updateUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func callFunctionWithValidationResult(function: () -> ValidationResult) {
        var result = function()
        
        if (result.resultType == ValidationResultType.Error) {
            toast?.error(result.message)
            return
        }
        
        updateUI()
    }

    private func updateUI() {
        dayLbl.text = "Day \(game.dayCount)"
        inventoryMoneyLbl.text = "$\(game.getMoney())"
        inventoryLemonsLbl.text = "x \(game.lemons.count)"
        inventoryIceCubesLbl.text = "x \(game.iceCubes.count)"
        purchasedLemonsLbl.text = "\(game.lemonsToPurchase)"
        purchasedIceCubesLbl.text = "\(game.iceCubesToPurchase)"
        mixedLemonsLbl.text = "\(game.getMixAmountOfLemons())"
        mixedIceCubesLbl.text = "\(game.getMixAmountOfIceCubes())"
        
        if (game.todaysWeather == WeatherType.Cold) {
            weatherImg.image = UIImage(named: "Cold")
        }
        else if (game.todaysWeather == WeatherType.Mild) {
            weatherImg.image = UIImage(named: "Mild")
        }
        else if (game.todaysWeather == WeatherType.Warm) {
            weatherImg.image = UIImage(named: "Warm")
        }
    }
}

