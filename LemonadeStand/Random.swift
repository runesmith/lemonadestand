//
//  Random.swift
//  LemonadeStand
//
//  Created by Thordur Thordarson on 2014-12-28.
//  Copyright (c) 2014 Thordur Thordarson. All rights reserved.
//

import Foundation

public class Random {
    public class func getNumber(minNumber: Int, maxNumber: Int) -> Int {
        var randomBase = Int(arc4random_uniform(maxNumber + 1))
        
        return randomBase + minNumber
    }
}