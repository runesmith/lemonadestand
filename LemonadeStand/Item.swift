//
//  Item.swift
//  LemonadeStand
//
//  Created by Thordur Thordarson on 2014-12-27.
//  Copyright (c) 2014 Thordur Thordarson. All rights reserved.
//

import Foundation

public class Item {
    public var text: String
    public var price: Int
    public var count: Int
    
    public init(text: String, price: Int, count: Int) {
        self.text = text
        self.price = price
        self.count = count
    }
}