//
//  Toast.swift
//  LemonadeStand
//
//  Created by Thordur Thordarson on 2014-12-27.
//  Copyright (c) 2014 Thordur Thordarson. All rights reserved.
//

import Foundation
import UIKit

public class Toast<TViewController: UIViewController> {
    private var viewController: TViewController
    
    init(viewController: TViewController) {
        self.viewController = viewController
    }
    
    public func success(message: String) {
        showMessage("Success", message: message)
    }
    
    public func info(header: String = "Info", message: String) {
        showMessage(header, message: message)
    }
    
    public func error(message: String) {
        showMessage("Error", message: message)
    }
    
    private func showMessage(header: String, message: String) {
        var alert = UIAlertController(title: header, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        viewController.presentViewController(alert, animated: true, completion: nil)
    }
}