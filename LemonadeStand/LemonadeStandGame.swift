//
//  LemonadeStandGame.swift
//  LemonadeStand
//
//  Created by Thordur Thordarson on 2014-12-27.
//  Copyright (c) 2014 Thordur Thordarson. All rights reserved.
//

import Foundation

public class LemonadeStandGame {
    public private(set) var lemons: Item
    public private(set) var iceCubes: Item
    public private(set) var lemonsToPurchase: Int
    public private(set) var iceCubesToPurchase: Int
    public private(set) var mixture: Mixture
    public private(set) var dayCount: Int
    public private(set) var todaysWeather: WeatherType
    
    private var inventory: Inventory
    
    public init (startMoney: Int = 10, startNumberOfLemons: Int = 1, startNumberOfIceCubes: Int = 1, priceOfLemon: Int = 2, priceOfIceCube: Int = 1) {
        dayCount = 1
        lemons = Item(text: "Lemons", price: priceOfLemon, count: startNumberOfLemons)
        iceCubes = Item(text: "Ice Cubes", price: priceOfIceCube, count: startNumberOfIceCubes)
        lemonsToPurchase = 0
        iceCubesToPurchase = 0
        mixture = Mixture(mixtureItems: lemons, iceCubes)
        todaysWeather = Weather.getRandomWeather()
        
        inventory = Inventory(startMoney: startMoney, items: lemons, iceCubes)
    }
    
    public func simulateDay() -> GameResult {
        var maxCustomers = 10
        
        if (todaysWeather == WeatherType.Cold) {
            maxCustomers -= 3
        }
        else if (todaysWeather == WeatherType.Warm) {
            maxCustomers += 4
        }
        
        return simulateDay(0, maxNumberOfCustomers: maxCustomers)
    }
    
    public func simulateDay(minNumberOfCustomers: Int, maxNumberOfCustomers: Int) -> GameResult {
        var drinkType = getLemonadeType()
        var numberOfCustomers = Random.getNumber(minNumberOfCustomers, maxNumber: maxNumberOfCustomers)
        var availableDrinks = getNumberOfDrinks()
        var drinksSold = 0
        var profit = 0
        
        for (var i = 0; i < numberOfCustomers; i++) {
            var customer = Customer()
            
            if (customerWillBuy(customer) && availableDrinks > 0) {
                availableDrinks -= 1
                drinksSold += 1
                profit += getPrice()
            }
        }
        
        inventory.addMoney(profit)
        
        mixture = Mixture(mixtureItems: lemons, iceCubes)
        
        dayCount += 1
        lemonsToPurchase = 0
        iceCubesToPurchase = 0
        todaysWeather = Weather.getRandomWeather()
        
        var summary = "\(numberOfCustomers) visited your lemonade stand today. You were able to sell \(drinksSold) drinks. Your total profit was $\(profit). Total number of drinks that was spoiled was \(availableDrinks)."
        
        if (inventory.getMoney() < lemons.price && lemons.count == 0) {
            return GameResult(resultType: GameResultType.GameOver, message: "You can't afford any more lemons! Summary of the day: \(summary)")
        }
        
        return GameResult(resultType: GameResultType.Proceed, message: summary)
    }
    
    public func getMoney() -> Int {
        return inventory.getMoney()
    }
    
    public func buyLemon() -> ValidationResult {
        var result = inventory.exchangeItemWithMoney(lemons, numberOfItems: 1)
        
        if (result.resultType == ValidationResultType.Success) {
            lemonsToPurchase++
        }
        
        return result
    }
    
    public func sellLemon() -> ValidationResult {
        var result = inventory.exchangeItemWithMoney(lemons, numberOfItems: -1)
        
        if (result.resultType == ValidationResultType.Success) {
            lemonsToPurchase--
        }
        
        return result
    }
    
    public func buyIceCubes() -> ValidationResult {
        var result = inventory.exchangeItemWithMoney(iceCubes, numberOfItems: 1)
        
        if (result.resultType == ValidationResultType.Success) {
            iceCubesToPurchase++
        }
        
        return result
    }
    
    public func sellIceCubes() -> ValidationResult {
        var result = inventory.exchangeItemWithMoney(iceCubes, numberOfItems: -1)
        
        if (result.resultType == ValidationResultType.Success) {
            iceCubesToPurchase--
        }
        
        return result
    }
    
    public func addLemonToMix() -> ValidationResult {
        return mixture.addToMixture(lemons, amount: 1)
    }
    
    public func removeLemonFromMix() -> ValidationResult {
        return mixture.removeFromMixture(lemons, amount: 1)
    }
    
    public func addIceCubesToMix() -> ValidationResult {
        return mixture.addToMixture(iceCubes, amount: 1)
    }
    
    public func removeIceCubesFromMix() -> ValidationResult {
        return mixture.removeFromMixture(iceCubes, amount: 1)
    }
    
    public func getMixAmountOfLemons() -> Int {
        return mixture.getAmount(lemons)
    }
    
    public func getMixAmountOfIceCubes() -> Int {
        return mixture.getAmount(iceCubes)
    }
    
    private func getRatioOfLemons() -> Double {
        return mixture.getRatioOfItem(lemons)
    }
    
    private func getLemonadeType() -> LemonadeType {
        var ratio = getRatioOfLemons()
        
        if (ratio > 1.0) {
            return LemonadeType.Acidic
        }
        else if (ratio < 1.0) {
            return LemonadeType.Diluted
        }
        else {
            return LemonadeType.EqualPortioned
        }
    }
    
    private func customerWillBuy(customer: Customer) -> Bool {
        var lemonadeType = getLemonadeType()
        
        return customer.preference == lemonadeType
    }
    
    private func getPrice() -> Int {
        return (getMixAmountOfLemons() * lemons.price + 1) + getMixAmountOfIceCubes()
    }
    
    private func getNumberOfDrinks() -> Int {
        return (getMixAmountOfLemons() * 2) + getMixAmountOfIceCubes()
    }
}

public enum LemonadeType {
    case Acidic
    case EqualPortioned
    case Diluted
}