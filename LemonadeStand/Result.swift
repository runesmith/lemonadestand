//
//  ValidationResult.swift
//  LemonadeStand
//
//  Created by Thordur Thordarson on 2014-12-27.
//  Copyright (c) 2014 Thordur Thordarson. All rights reserved.
//

import Foundation

public class Result {
    public var message: String
    
    public init (message: String = "") {
        self.message = message
    }
}

public class GameResult: Result {
    public var resultType: GameResultType
    
    public init (resultType: GameResultType, message: String = "") {
        self.resultType = resultType
        super.init(message: message)
    }
}

public enum GameResultType {
    case Proceed
    case GameOver
}

public class ValidationResult: Result {
    public var resultType: ValidationResultType
    
    public init (resultType: ValidationResultType, message: String = "") {
        self.resultType = resultType
        super.init(message: message)
    }
}

public enum ValidationResultType {
    case Success
    case Error
}