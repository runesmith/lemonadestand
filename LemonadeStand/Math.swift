//
//  Math.swift
//  LemonadeStand
//
//  Created by Thordur Thordarson on 2014-12-28.
//  Copyright (c) 2014 Thordur Thordarson. All rights reserved.
//

import Foundation

protocol Math {
    class func add(lhs: Self, rhs: Self) -> Self
    class func substract(lhs: Self, rhs: Self) -> Self
    class func multiply(lhs: Self, rhs: Self) -> Self
    class func divide(lhs: Self, rhs: Self) -> Self
    class func zero() -> Self
}

extension Int: Math {
    static func add(lhs: Int, rhs: Int) -> Int {
        return lhs + rhs
    }
    
    static func substract(lhs: Int, rhs: Int) -> Int {
        return lhs - rhs
    }
    
    static func multiply(lhs: Int, rhs: Int) -> Int {
        return lhs * rhs
    }
    
    static func divide(lhs: Int, rhs: Int) -> Int {
        return lhs / rhs
    }
    
    static func zero() -> Int {
        return 0
    }
}