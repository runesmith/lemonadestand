//
//  Weather.swift
//  LemonadeStand
//
//  Created by Thordur Thordarson on 2014-12-28.
//  Copyright (c) 2014 Thordur Thordarson. All rights reserved.
//

import Foundation

public class Weather {
    public class func getRandomWeather() -> WeatherType {
        var randomNumber = Random.getNumber(1, maxNumber: 3)
        
        if (randomNumber == 1) {
            return WeatherType.Warm
        }
        else if (randomNumber == 2) {
            return WeatherType.Mild
        }
        else {
            return WeatherType.Cold
        }
    }
}

public enum WeatherType {
    case Warm
    case Mild
    case Cold
}