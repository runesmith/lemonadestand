//
//  Inventory.swift
//  LemonadeStand
//
//  Created by Thordur Thordarson on 2014-12-27.
//  Copyright (c) 2014 Thordur Thordarson. All rights reserved.
//

import Foundation

public class Inventory {
    private var money: Int
    private var items: [Item]
    
    init (startMoney: Int = 10, items: Item...) {
        self.money = startMoney
        self.items = items
    }
    
    public func getMoney() -> Int {
        return money;
    }
    
    public func addMoney(amount: Int) {
        money += amount
    }
    
    public func exchangeItemWithMoney(var item: Item, numberOfItems: Int) -> ValidationResult {
        var updatedItemCount = numberOfItems + item.count
        
        if (updatedItemCount < 0) {
            return ValidationResult(resultType: ValidationResultType.Error, message: "You are \(-updatedItemCount) \(item.text) short of making this transaction")
        }
        
        var cost = item.price * numberOfItems
        
        if (cost > money) {
            return ValidationResult(resultType: ValidationResultType.Error, message: "You are $\(cost - money) short of making this transaction")
        }
        
        money -= cost
        item.count += numberOfItems
        
        return ValidationResult(resultType: ValidationResultType.Success)
    }
}