//
//  Array.swift
//  LemonadeStand
//
//  Created by Thordur Thordarson on 2014-12-28.
//  Copyright (c) 2014 Thordur Thordarson. All rights reserved.
//

import Foundation

extension Array {
    func contains<T where T : AnyObject>(obj: T) -> Bool {
        return self.filter({$0 as? T === obj}).count > 0
    }
    
    func indexOf<T where T : AnyObject>(obj: T) -> Int {
        for (var i = 0; i < self.count; i++) {
            if (self[i] as T === obj) {
                return i
            }
        }
        
        return -1
    }
    
    func sum<T : Math>() -> T {
        return self.map { $0 as T }.reduce(T.zero()) { T.add($0, rhs: $1) }
    }
}